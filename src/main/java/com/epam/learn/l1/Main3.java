package com.epam.learn.l1;

public class Main3 {
    Byte aByte;
    Short aShort;
    Integer aInteger;
    Long aLong;

    Float aFloat;
    Double aDouble;

    Boolean aBoolean;
    Character character;

    public static void main(String[] args) {
        // контрол + альт + L (аккуратно использовать!!!)
        // контрол + шифт + альт + L
        int a = 5;
        System.out.println(String.valueOf(a));
        System.out.println(Integer.valueOf("5") - 2);
        System.out.println(Integer.valueOf("5") + 2);
        System.out.println(Integer.reverse(521));
        System.out.println(new StringBuilder("hi").reverse().toString());
        System.out.println(new Main3().character);
    }

    private int getSum(int a) {
        return 1;
    }

    private int getSumOfAB(int a, int b) {
        return a + b;
    }

}
